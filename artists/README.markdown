## Context-Free

- [Site](https://www.contextfreeart.org/)
- [Documentation](https://github.com/MtnViewJohn/context-free/wiki)
- [Gallery](https://www.contextfreeart.org/gallery/search.php?t=faves&num=5)

## Raven Kwok

- [Portfolio](http://ravenkwok.com/)
- [Gallery - Vimeo](https://vimeo.com/ravenkwok)
- [Open Processing](https://www.openprocessing.org/user/12203)

## Matt Pearson

- [Portfolio](http://zenbullets.com/images.php)
- [Gallery - Vimeo](http://zenbullets.com/video.php)
- [Generative Art - code](https://github.com/samaaron/gen-art)
- [Generative Art - book](http://zenbullets.com/book.php)

- [Abandoned Art](http://abandonedart.org/)

## Daniel Shiffman

- [Nature of Code - Book](http://natureofcode.com/)
- [Coding Challenges](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH)

## Coding Math

- [Coding Math](https://www.youtube.com/channel/UCF6F8LdCSWlRwQm_hfA2bcQ)

## Casey Reass

- [Creator's project](https://www.youtube.com/channel/UC_NaA2HkWDT6dliWVcvnkuQ)
- [Reass](http://reas.com/)
- [Gallery - Vimeo](https://www.google.co.in/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwissc61h9zQAhXFo48KHYNZDG0QFgg-MAQ&url=https%3A%2F%2Fvimeo.com%2Freas&usg=AFQjCNHWfesGp5uh1kRzY95fZDbhWqcuIw&sig2=5IHp2qT-ltcN587UJVrJ5A)

## Rudy Rucker \& John Walker

- [Exploring Cellular Automata](https://www.fourmilab.ch/cellab/manual/)
- [Rudy Rucker's Gallery - Youtube](https://www.youtube.com/user/rudyrucker/playlists)
- [CA - docu](https://www.youtube.com/watch?v=lyZUzakG3bE&list=PLwqJQofx7awcVUc0UHOENLNUmjyaR_3iR&index=1&t=1250s)

## Cellular Automata

- [List of CA software](http://uncomp.uwe.ac.uk/genaro/Cellular_Automata_Repository/Software.html)

## Jon Mccormack

- [Artworks](http://jonmccormack.info/artworks/)
