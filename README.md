# Programming Reality

**or creative coding for hedonists**

1. Layered Model of the World - physical, bilogical, genetic, neural, economic, mathematical, computational
2. Coding for simulation and coding for visualization
3. Everything as a network
4. Chaos theory - reality as a causal chain