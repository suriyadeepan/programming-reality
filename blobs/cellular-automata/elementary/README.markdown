**Source Material**

1. [Introduction to Cellular Automata](https://www.youtube.com/watch?v=DKGodqDs9sA)
2. [John Conway's Game of Life](https://www.youtube.com/watch?v=CgOcEZinQ2I&index=1&list=PLWZrpIHt9H2J7W-Tyx2AdiqbRwv58BTKq)
3. [Wolfram Elementary Cellular Automaton](http://mathworld.wolfram.com/ElementaryCellularAutomaton.html)
4. [Nature of Code - Cellular Automata](http://natureofcode.com/book/chapter-7-cellular-automata/)
5. [**Video** Nature of Code - Cellular Automata](https://www.youtube.com/watch?v=W1zKu3fDQR8)
6. [Complexity Explorer](https://www.complexityexplorer.org/courses/59-introduction-to-complexity-fall-2016/segments/4465)