# Game of Life

![](GoL.gif)

**Source Material**

1. [Nature of Code - Game of Life](https://www.youtube.com/watch?v=DKGodqDs9sA)
2. [Math Games - Game of Life](http://www.ibiblio.org/lifepatterns/october1970.html)


## Rules

1. Death  : (1->0) {num of neighbors} >= 4 || <= 1
2. Birth  : (0->1) {num of neighbors}  = 3
3. Stasis : (x->x) **else**
