var grid = [];
var grid_steps = 100;
var t = 0;
// assign rule set here
var ruleset_dec = 30;

/*

 |------------properties------------|

 */
var bgColor = 35;
// origin
var originX = 0;
var originY = 0;
// define grid dimensions
var rows = 0;
var cols = 0;
// initial amount of live cells
var threshold = 0.1; // 0-1


function mousePressed(){
	grid.random(threshold);
}

function setup(){
	createCanvas(windowWidth,windowHeight);
	background(bgColor);

	// setup origin
	originX = windowWidth*0.1;
	originY = windowHeight*0.1;

	// setup grid dimensions
	rows = int( (windowWidth/5) * 0.8); // 80% of window
	cols = int( (windowHeight/5) *0.8);

	translate(originX,originY);
	grid = new Grid( matrix(rows,cols,1), 1 );
	grid.random(threshold);
	grid.show();
}


function draw(){
	
	background(bgColor);
	translate(originX,originY);
	grid = grid.next_gen();
	grid.show();
	
}
