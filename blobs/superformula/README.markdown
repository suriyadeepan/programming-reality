**Source Material**

* [Trippy Animations with superformula](https://www.youtube.com/watch?v=u6arTXBDYhQ)

## Polar to Cartesian

x = r*cos(theta)
y = r*sin(theta)
