// initialize t
var t = 0;
// num of lines
var num_lines = 10;

function setup(){
	createCanvas(600,600);
	//background(50);
	noFill();
	stroke(255);
	strokeWeight(2);
	
}

function draw(){
	translate(width/2,height/2);
	background(50);

	beginShape();
	// add some vertices
	for(var theta=0;theta<=2*PI;theta+=0.01){
		var rad = r(theta,
			4, // a
			4, // b
			6, // m
			1, // n1
			sin(t)*0.5 + 0.5, // n2
			cos(t)*0.5 + 0.5); // n3
		var x = rad*cos(theta) *50;
		var y = rad*sin(theta) *50;
		vertex(x,y);
	}
	endShape();
	t+=0.01;
}

function r(theta,a,b,m,n1,n2,n3){
	return pow(pow(abs(cos(m*theta/4.0)/a),n2) +
	 pow(abs(sin(m*theta/4.0)/b),n3), -1.0/n1);
}

