// initialize t
var t = 0;
// num of lines
var num_lines = 10;

function setup(){
	createCanvas(600,600);
	//background(50);
	stroke(255);
	strokeWeight(5);
}

function draw(){
	// clear screen
	background(50);

	// translate origin
	translate(width/2, height/2);
	// draw a point
	//point(x1(t), y1(t));
	//point(x2(t), y2(t));

	// draw a line connecting points
	//line(x1(t), y1(t), x2(t), y2(t));

	// need multiple lines
	for(var i=0;i<num_lines;i++){
		line(x1(t+i), y1(t+i), x2(t+i), y2(t+i));
	}

	// time count (frame count)
	t += 0.5;
}

function x1(t){	return sin(t/10)*100 + sin(t/5)*20; }
function y1(t){	return cos(t/10)*100; }

function x2(t){	return sin(t/10)*200 + sin(t)*2; }
function y2(t){	return cos(t/20)*200 + cos(t/12)*20; }