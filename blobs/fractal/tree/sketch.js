var angle;
var slider;

function setup(){
	createCanvas(600,600);
	// create slider to adjust angle
	slider = createSlider(0,TWO_PI,PI/4,0.01);


	//background(50);
	noFill();
	stroke(255);
	strokeWeight(2);
	
	// init angle
	angle = PI/4;
}

function draw(){

	background(50);
	stroke(255);
	translate(300,height);
	branch(120);

}

function branch(len){
	line(0,0,0,-len);
	translate(0,-len);
	
	// get angle from slider
	angle = slider.value();

	
	if(len>4){
		push();
		rotate(angle);
		branch(len*0.67);
		pop();

		push();
		rotate(-angle);
		branch(len*0.67);
		pop();
	}
	//line(0,0,0,-len*2.0/3.0);
}


