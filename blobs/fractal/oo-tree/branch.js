// constructor
function Branch(begin,end){
	this.begin = begin;
	this.end = end;

	// add boolean for finished branch
	this.finished = false;

	// display tree
	this.show = function(){
		stroke(255);
		line(this.begin.x, this.begin.y, this.end.x, this.end.y);
	}

	// spawn new branch
	this.branch = function(angle){
		// create a direction vector
		var dir = p5.Vector.sub(this.end, this.begin);
		// rotate direction vector
		dir.rotate(angle);
		// shrink vector magnitude 0.67 times
		dir.mult(0.67);

		// create new end point
		var new_end = p5.Vector.add(this.end, dir);

		// new branch
		new_branch = new Branch(this.end, new_end);

		return new_branch;
	}

	// spawn right branch
	this.right = function(){
		return this.branch(PI/4);
	}

	// spawn left branch
	this.left = function(){
		return this.branch(-PI/4);
	}	

}
