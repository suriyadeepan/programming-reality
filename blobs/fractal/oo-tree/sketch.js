var tree = [];

function setup(){

	createCanvas(600,600);
	// start and end points of branch
	a = createVector(width/2, height);
	b = createVector(width/2, height-100);

	// create branch
	var root = new Branch(a,b);

	// add root to tree
	tree[0] = root;

}

function mousePressed(){
	// get size of tree
	tree_len = tree.length;

	for(var i=0;i<tree_len;i++){
		// check if tree has branches
		if(!tree.finished){
			// spawn left and right branches
			// 	add new branches to tree array
			tree.push(tree[i].left() );
			tree.push(tree[i].right());	
		}
		// now it is
		tree[i].finished = true;
	}
	
}

function draw(){

	background(50);
	for(var i=0;i<tree.length;i++){
		tree[i].show();
	}
	
}

