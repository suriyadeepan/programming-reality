# Evolution

A definition of evolution,

> Change in allele frequencies in a population over time.

Information of all life is stored in DNA. Streches of DNA are called genes. Each gene encodes information which can create proteins. These protiens create cells that make up everything.

 - Genes come in different versions : **allele**

Forces that affect allele frequency

1. Natural selection
2. Genetic Drift
3. Migration
4. Mutation

## Models of Evolutionary Biology


### Null Model

force ---> consequences

 - remove all the forces => null model
 - **Hardy-Weinberg** model
 - diploid (2 copies of DNA) vs haploid (1)
 - most animals are diploid
 - some of the cells are haploid - sex cells (gamets)
 - meiosis creates haploid cells => gamets (for sexual reproduction)
 - 2 indivduals engage in sexual reproductions by fusing of gametes => new cell is formed, zygote(diploid)
 - male gamets (sperm cells) vs female gamets (eggs)
 - 1 gene (A) - (i) Allele A1 and ii) Allele A2 => can package into diploid cells in 3 ways (A1A1, A2A2, A1A2)
 - A1A1, A2A2 => homozygous, A1A2 => heterozygous
 - 3 sets of alleles => genotypes
 - assume 100 individuals 
    - A1A1 : 15
    - A1A2 : 50
    - A2A2 : 35
    - Allele frequency : A1 = (30+50)/200 = 40%, A2 = 60%
    - **Next Generation**
        - A1A1 : 0.4*0.4 = 0.16 ( increase of 1%)
        - A1A2 : 0.4*0.6 + 0.6*0.4 = 0.48 ( decrease of 2%)
        - A2A2 : 0.6*0.6 = 0.36 (increase of 1%)
        - Allele frequency : A1 = 16+24 = 40%, A2 = 60%
        - **No change in allele frequency** : **No evolution**
        - 16%, 48%, 36% : **Hardy-Weinberg frequencies** - whatever frequencies we start with, within one generation, it will go to HW frequencies
        
 


## Jargons

 - allele